package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.Career;
import uz.pdp.kokanduni.entity.Direction;

@Projection(name = "customDirection", types = Direction.class)
public interface CustomDirection {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
