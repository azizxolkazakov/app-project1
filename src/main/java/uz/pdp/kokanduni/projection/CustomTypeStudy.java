package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.Career;
import uz.pdp.kokanduni.entity.TypeStudy;

@Projection(name = "customTypeStudy", types = TypeStudy.class)
public interface CustomTypeStudy {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
