package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.Career;
import uz.pdp.kokanduni.entity.District;

@Projection(name = "customCareer", types = Career.class)
public interface CustomCareer {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
