package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.District;
import uz.pdp.kokanduni.entity.Gender;

@Projection(name = "customGender", types = Gender.class)
public interface CustomGender {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
