package uz.pdp.kokanduni.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.District;

@Projection(name = "customDistrict", types = District.class)
public interface CustomDistrict {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();

    @Value("#{target.region.id}")
    Integer getRegionId();

    @Value("#{target.region.nameUz}")
    String getRegionNameUz();

    @Value("#{target.region.nameRu}")
    String getRegionNameRu();

    @Value("#{target.region.nameEn}")
    String getRegionNameEn();
}
