package uz.pdp.kokanduni.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.kokanduni.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Gender extends AbsNameEntity {
}
