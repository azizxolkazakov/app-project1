package uz.pdp.kokanduni.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.kokanduni.entity.enums.AttachmentTypeEnum;
import uz.pdp.kokanduni.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class AttachmentType extends AbsEntity {
    private String contentTypes;
    @Enumerated(value = EnumType.STRING)
    private AttachmentTypeEnum attachmentTypeEnum;
    private int width;
    private int height;
    private Long size;
}
