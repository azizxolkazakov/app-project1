package uz.pdp.kokanduni.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.kokanduni.entity.template.AbsNameEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class PassportInfo extends AbsNameEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;
    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;
    @ManyToOne(fetch = FetchType.LAZY)
    private District district;
    @ManyToOne
    private Nation nation;
    private String address;
    private String passportSeries;
    private String addressPassport;
}
