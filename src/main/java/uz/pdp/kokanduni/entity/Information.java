package uz.pdp.kokanduni.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Information {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne(fetch = FetchType.LAZY)
    private Career career;
    @ManyToOne
    private TypeStudy typeStudy;
    private String studyName;
    private String endStudyYear;
    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;
    @ManyToOne(fetch = FetchType.LAZY)
    private District district;

}
