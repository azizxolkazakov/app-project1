package uz.pdp.kokanduni.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.kokanduni.entity.template.AbsNameEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class District extends AbsNameEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private Region region;
    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    private List<PassportInfo> passportInfos;
}
