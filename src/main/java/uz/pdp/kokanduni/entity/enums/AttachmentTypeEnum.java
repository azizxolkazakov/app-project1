package uz.pdp.kokanduni.entity.enums;

public enum AttachmentTypeEnum {
    AVATAR, LOGO, VIDEO, PASSPORT
}
