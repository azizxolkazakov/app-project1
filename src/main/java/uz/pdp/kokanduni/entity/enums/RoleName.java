package uz.pdp.kokanduni.entity.enums;

public enum RoleName {
    ROLE_USER, ROLE_MANAGER, ROLE_DIRECTOR, ROLE_ADMIN
}
