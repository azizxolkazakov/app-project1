package uz.pdp.kokanduni.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.pdp.kokanduni.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "users")
public class User extends AbsEntity implements UserDetails {

    private String lastName;
    private String firstName;
    private String middleName;
    private Date birthDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private Gender genders;
    @OneToOne(fetch = FetchType.LAZY)
    private Attachment photo;
    @ManyToOne(fetch = FetchType.LAZY)
    private PassportInfo passportInfo;
    @ManyToOne(fetch = FetchType.LAZY)
    private Information information;
    @ManyToOne(fetch = FetchType.LAZY)
    private Communication communication;
    @ManyToOne(fetch = FetchType.LAZY)
    private Direction direction;
    @ManyToOne(fetch = FetchType.LAZY)
    private LanguageEducation languageEducation;
    @ManyToOne(fetch = FetchType.LAZY)
    private ForeignLanguage foreignLanguage;

    private String phoneNumber;
    private String password;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    private List<Role> roles;

    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    public User(String firstName, String lastName, String phoneNumber, String password, List<Role> roles) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.roles = roles;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public String getUsername() {
        return this.phoneNumber;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }
}
