package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.kokanduni.entity.Communication;

public interface CommunicationRepository extends JpaRepository<Communication, Integer> {
}
