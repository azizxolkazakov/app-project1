package uz.pdp.kokanduni.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.kokanduni.entity.Attachment;
import uz.pdp.kokanduni.entity.AttachmentContent;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    AttachmentContent findByAttachment(Attachment attachment);
}
