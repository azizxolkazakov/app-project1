package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.kokanduni.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumber(String s);
    List<User> findAllByEnabled(boolean enabled);

    List<User> findAllByCreatedBy(UUID createdBy);
    boolean existsByPhoneNumber(String phoneNumber);
}
