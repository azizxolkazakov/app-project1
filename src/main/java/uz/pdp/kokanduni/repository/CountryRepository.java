package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.pdp.kokanduni.entity.Country;
import uz.pdp.kokanduni.projection.CustomCountry;

import java.util.List;

@RepositoryRestResource(path = "/country", excerptProjection = CustomCountry.class)
public interface CountryRepository extends JpaRepository<Country, Integer> {

    @RestResource(path = "/serial")
    @Query(value = "select * from region", nativeQuery = true)
    List<Country> getSerialCountry();
}
//Country data-rest