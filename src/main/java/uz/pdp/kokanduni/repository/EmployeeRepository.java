package uz.pdp.kokanduni.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Employee;
import uz.pdp.kokanduni.projection.CustomEmployee;


import java.util.UUID;

@RepositoryRestResource(path = "/employee",excerptProjection = CustomEmployee.class)
public interface EmployeeRepository extends JpaRepository<Employee, UUID> {
}
