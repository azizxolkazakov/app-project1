package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.kokanduni.entity.PassportInfo;

public interface PassportInfoRepository extends JpaRepository<PassportInfo, Integer> {
}
