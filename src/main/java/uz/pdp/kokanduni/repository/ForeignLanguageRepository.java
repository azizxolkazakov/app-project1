package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Direction;
import uz.pdp.kokanduni.entity.ForeignLanguage;
import uz.pdp.kokanduni.projection.CustomDirection;
import uz.pdp.kokanduni.projection.CustomForeignLanguage;

@RepositoryRestResource(path = "/foreignLanguage",excerptProjection = CustomForeignLanguage.class)
public interface ForeignLanguageRepository extends JpaRepository<ForeignLanguage,Integer> {
}
