package uz.pdp.kokanduni.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.kokanduni.entity.AttachmentType;
import uz.pdp.kokanduni.entity.enums.AttachmentTypeEnum;

import java.util.UUID;

public interface AttachmentTypeRepository extends JpaRepository<AttachmentType, UUID> {
    AttachmentType findByAttachmentTypeEnum(AttachmentTypeEnum attachmentTypeEnum);
}
