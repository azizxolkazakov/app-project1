package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.kokanduni.entity.Information;

public interface InformationRepository extends JpaRepository<Information, Integer> {
}
