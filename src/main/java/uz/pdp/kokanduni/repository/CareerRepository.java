package uz.pdp.kokanduni.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Career;
import uz.pdp.kokanduni.projection.CustomCareer;

@RepositoryRestResource(path = "/career",excerptProjection = CustomCareer.class)
public interface CareerRepository extends JpaRepository<Career, Integer> {
}
