package uz.pdp.kokanduni.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import uz.pdp.kokanduni.service.AttachmentService;

import java.util.UUID;

@RestController
@RequestMapping("/api/attach")
public class AttachmentController {
    @Autowired
    AttachmentService attachmentService;

    @PostMapping
    public HttpEntity<?> uploadFile(MultipartHttpServletRequest request) {
        return ResponseEntity.ok(attachmentService.saveFile(request));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable UUID id) {
        return attachmentService.getFile(id);
    }
}
