package uz.pdp.kokanduni.security;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import uz.pdp.kokanduni.entity.Attachment;
import uz.pdp.kokanduni.entity.AttachmentContent;
import uz.pdp.kokanduni.entity.Country;
import uz.pdp.kokanduni.entity.User;
import uz.pdp.kokanduni.entity.enums.RoleName;
import uz.pdp.kokanduni.payload.ApiResponse;
import uz.pdp.kokanduni.payload.ReqUser;
import uz.pdp.kokanduni.repository.*;
import uz.pdp.kokanduni.service.CommunicationService;
import uz.pdp.kokanduni.service.InformationService;
import uz.pdp.kokanduni.service.PassportInfoService;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class AuthService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PassportInfoService passportInfoService;
    @Autowired
    InformationService informationService;
    @Autowired
    CommunicationService communicationService;
    @Autowired
    GenderRepository genderRepository;
    @Autowired
    AttachmentContentRepository attachmentContentRepository;


    public ApiResponse addUser(ReqUser reqUser) {
        if (!userRepository.existsByPhoneNumber(reqUser.getReqCommunication().getPhoneNumber())) {
            User user = new User();
            user.setLastName(reqUser.getLastName());
            user.setFirstName(reqUser.getFirstName());
            user.setMiddleName(reqUser.getMiddleName());
            user.setBirthDate(reqUser.getBirthDate());
            user.setGenders(genderRepository.getOne(reqUser.getGenderId()));
            user.setPhoto(reqUser.getPhotoId() == null ? null : attachmentRepository.getOne(reqUser.getPhotoId()));
            user.setPassportInfo(passportInfoService.addPassportInfo(reqUser.getReqPassportInfo()));
            user.setInformation(informationService.addInformation(reqUser.getReqInformation()));
            user.setCommunication(communicationService.addCommunication(reqUser.getReqCommunication()));
            user.setPhoneNumber(reqUser.getReqCommunication().getPhoneNumber());
            user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
            user.setRoles(Collections.singletonList(roleRepository.findByRoleName(RoleName.ROLE_USER)));
            userRepository.save(user);
            return new ApiResponse("User ro'yxatdan o'tkazildi", true, reqUser.getPassword());
        }
        return new ApiResponse("Bunday telefon raqamli user sistemada mavjud", false);
    }


    public ApiResponse getUsersExcel() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        String currentDate = simpleDateFormat.format(new Date());
        List<User> users = userRepository.findAll();
        String title = "Kompanyalar haqida ma'lumot. " + currentDate;
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(title);
        sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 8));

        XSSFRow rowHead = sheet.createRow((short) 0);
        rowHead.setHeightInPoints(30);
        XSSFCell cellHead = rowHead.createCell(0);
        cellHead.setCellValue(title);
        XSSFFont font = workbook.createFont();
        font.setColor((short) 10);
        font.setFontHeightInPoints((short) 22);
        font.setColor(IndexedColors.WHITE.getIndex());
        XSSFCellStyle xssfCellStyle = workbook.createCellStyle();
        xssfCellStyle.setFont(font);
        rowHead.setRowStyle(xssfCellStyle);

        CellStyle styleHead = workbook.createCellStyle();
        styleHead.setBorderBottom(BorderStyle.THIN);
        styleHead.setFillForegroundColor(IndexedColors.DARK_BLUE.getIndex());
        styleHead.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        styleHead.setAlignment(HorizontalAlignment.CENTER);
        styleHead.setFont(font);
        cellHead.setCellStyle(styleHead);
        CellStyle style = workbook.createCellStyle();
        style.setBorderBottom(BorderStyle.THIN);
        style.setAlignment(HorizontalAlignment.CENTER);
        XSSFFont font1 = workbook.createFont();
        font1.setFontHeightInPoints((short) 14);
        style.setFont(font1);
        CellStyle styleCol = workbook.createCellStyle();
        styleCol.setAlignment(HorizontalAlignment.CENTER);
        XSSFFont fontCol = workbook.createFont();
        styleCol.setFont(fontCol);

        XSSFRow rowHeader = sheet.createRow((short) 1);
        Cell cell = rowHeader.createCell(0);
        cell.setCellValue("ismi");
        cell = rowHeader.createCell(1);
        cell.setCellValue("familyasi");

        for (int i = 0; i < users.size(); i++) {
            XSSFRow row = sheet.createRow((short) i + 2);
            cell = row.createCell(0);
            cell.setCellValue(users.get(i).getFirstName());
            cell = row.createCell(1);
            cell.setCellValue(users.get(i).getLastName());
        }
        try {
            File file = new File("report3.xlsx");
            OutputStream outputStream = new FileOutputStream(file);
            InputStream inputStream = new FileInputStream(file);
            Attachment attachment = new Attachment();
            attachment.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            attachment.setName(file.getName());
            workbook.write(outputStream);
            workbook.close();
            attachment.setSize(file.length());
            Attachment savedAttachment = attachmentRepository.save(attachment);
            AttachmentContent attachmentContent = new AttachmentContent();
            attachmentContent.setAttachment(savedAttachment);
            byte[] bytes = IOUtils.toByteArray(inputStream);
            attachmentContent.setBytes(bytes);
            attachmentContentRepository.save(attachmentContent);
            return new ApiResponse("Mana excel", true, ServletUriComponentsBuilder.fromCurrentContextPath().
                    path("/api/attach/").path(String.valueOf(savedAttachment.getId())).toUriString());
        } catch (Exception e) {
            return null;
        }
    }



    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(s).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

    public UserDetails loadUserByUserId(UUID id) {
        return userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException("User id no validate"));
    }

}
