package uz.pdp.kokanduni.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqInformation {
    private Integer careerId;
    private Integer typeStudyId;
    private String studyName;
    private String endStudyYear;
    private Integer regionIdInfo;
    private Integer districtIdInfo;
}
