package uz.pdp.kokanduni.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.Random;
import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqUser {
    private String password=alphaNumericString(6);
    private String lastName;
    private String firstName;
    private String middleName;
    private Date birthDate;
    private Integer genderId;
    private UUID photoId;
    private ReqPassportInfo reqPassportInfo;
    private ReqInformation reqInformation;
    private ReqCommunication reqCommunication;
    private Integer directionId;
    private Integer languageEducationId;
    private Integer foreignLanguageId;
    public static String alphaNumericString(int len) {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random rnd = new Random();

        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
}
