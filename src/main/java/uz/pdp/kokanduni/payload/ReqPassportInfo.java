package uz.pdp.kokanduni.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqPassportInfo {
    private Integer countryId;
    private Integer regionId;
    private Integer districtId;
    private Integer nationId;
    private String homeAddress;
    private String passportSeries;
    private String passportAddress;
    private UUID passportPdfId;
}
