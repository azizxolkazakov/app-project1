package uz.pdp.kokanduni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.kokanduni.entity.Communication;
import uz.pdp.kokanduni.payload.ReqCommunication;
import uz.pdp.kokanduni.repository.CommunicationRepository;

@Service
public class CommunicationService {
    @Autowired
    CommunicationRepository communicationRepository;

    public Communication addCommunication(ReqCommunication reqCommunication) {
        Communication communication = new Communication();
        communication.setPhoneNumber(reqCommunication.getPhoneNumber());
        communication.setHomePhoneNumber(reqCommunication.getHomePhoneNumber());
        communication.setEmail(reqCommunication.getEmail());
        return communicationRepository.save(communication);
    }
}
