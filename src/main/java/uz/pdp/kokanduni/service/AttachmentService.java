package uz.pdp.kokanduni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.kokanduni.entity.Attachment;
import uz.pdp.kokanduni.entity.AttachmentContent;
import uz.pdp.kokanduni.entity.AttachmentType;
import uz.pdp.kokanduni.entity.enums.AttachmentTypeEnum;
import uz.pdp.kokanduni.payload.ApiResponse;
import uz.pdp.kokanduni.repository.AttachmentContentRepository;
import uz.pdp.kokanduni.repository.AttachmentRepository;
import uz.pdp.kokanduni.repository.AttachmentTypeRepository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

@Service
public class AttachmentService {
    private final AttachmentRepository attachmentRepository;
    private final AttachmentContentRepository attachmentContentRepository;
    private final AttachmentTypeRepository attachmentTypeRepository;

    public AttachmentService(AttachmentRepository attachmentRepository, AttachmentContentRepository attachmentContentRepository, AttachmentTypeRepository attachmentTypeRepository) {
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
        this.attachmentTypeRepository = attachmentTypeRepository;
    };

    public UUID saveFile(MultipartHttpServletRequest request) {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile multipartFile = request.getFile(fileNames.next());
        Attachment attachment = new Attachment(
                multipartFile.getOriginalFilename(),
                multipartFile.getContentType(),
                multipartFile.getSize()
        );
        Attachment savedAttachment = attachmentRepository.save(attachment);
        try {
            AttachmentContent attachmentContent = new AttachmentContent(
                    savedAttachment,
                    multipartFile.getBytes()
            );
            attachmentContentRepository.save(attachmentContent);
            return savedAttachment.getId();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAttachment"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachment(attachment);
        return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+attachment.getName()+"\"")
                .body(attachmentContent.getBytes());
    }
}
