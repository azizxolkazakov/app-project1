package uz.pdp.kokanduni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.kokanduni.entity.PassportInfo;
import uz.pdp.kokanduni.payload.ReqPassportInfo;
import uz.pdp.kokanduni.repository.*;

@Service
public class PassportInfoService {
    @Autowired
    NationRepository nationRepository;
    @Autowired
    CountryRepository countryRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    PassportInfoRepository passportInfoRepository;


    public PassportInfo addPassportInfo(ReqPassportInfo reqPassportInfo) {
        PassportInfo passportInfo = new PassportInfo();
        passportInfo.setCountry(countryRepository.getOne(reqPassportInfo.getCountryId()));
        passportInfo.setRegion(regionRepository.getOne(reqPassportInfo.getRegionId()));
        passportInfo.setDistrict(districtRepository.getOne(reqPassportInfo.getDistrictId()));
        passportInfo.setNation(nationRepository.getOne(reqPassportInfo.getNationId()));
        passportInfo.setAddress(reqPassportInfo.getHomeAddress());
        passportInfo.setPassportSeries(reqPassportInfo.getPassportSeries());
        passportInfo.setAddressPassport(reqPassportInfo.getPassportAddress());
        return passportInfoRepository.save(passportInfo);
    }
}
